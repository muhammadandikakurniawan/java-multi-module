package com.javadevjournal.service;

import java.util.*;

import org.springframework.stereotype.Service;

@Service("article_service")
public class ArticleService {
    
    public List<HashMap<String,Object>> getAll(){

        List<HashMap<String,Object>> datas = new ArrayList<>();
        datas.add(createArticle("title 1","content 1"));
        datas.add(createArticle("title 2","content 2"));
        datas.add(createArticle("title 3","content 3"));
        return datas;
    }

    private HashMap<String,Object> createArticle(String title, String content){
        return new HashMap<String,Object>(){{
            put("title", title);
            put("content", content);
        }};
    }

}
