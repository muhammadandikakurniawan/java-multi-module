package com.javadevjournal.controller;

import java.util.*;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;
import com.javadevjournal.service.*;

@RestController()
@RequestMapping("article")
public class ArticleController {

    // @Qualifier("article_service")
    // @Autowired
    @Resource(name="article_service")
    ArticleService articleService;
    
    @RequestMapping(value = "get-all", method = RequestMethod.GET)
    public List<HashMap<String,Object>> getAll(){
        return articleService.getAll();
    }

}
